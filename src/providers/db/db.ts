import {
  HttpClient
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';
import {
  PreguntaModel
} from '../../pages/pregunta/preguntaModel';

@Injectable()
export class DbProvider {

  public preguntas: Array < PreguntaModel > ;
  public tipo: string;
  public index: number;
  postURL = "http://hackatontsdl.mybluemix.net/reports";

  constructor(public http: HttpClient) {

    this.preguntas = this.crearPreguntas();
    this.index = 0;

  }
  
  public crearPreguntas(): Array < PreguntaModel > {

    let preguntas: Array < PreguntaModel > = [];

    //PREGUNTAS PARA ADULTOS

    if(this.tipo == 'adultos') {
      console.log('adultos');

    let pregunta1 = new PreguntaModel("¿Cuál es su género?", "./assets/imgs/iconos/computador.png");
    pregunta1.crearOpcion("masculino")
    pregunta1.crearOpcion("femenino")
    preguntas.push(pregunta1);

    let pregunta2 = new PreguntaModel("¿Tiene golpes o moretones visibles en su cuerpo?", "./assets/imgs/iconos/computador.png");
    pregunta2.crearOpcion("si")
    pregunta2.crearOpcion("no")
    preguntas.push(pregunta2);

    let pregunta3 = new PreguntaModel("¿Sus respuestas parecen aprendidas de memoria?", "./assets/imgs/iconos/computador.png");
    pregunta3.crearOpcion("si")
    pregunta3.crearOpcion("no")
    pregunta3.crearOpcion("sininfo")
    preguntas.push(pregunta3);

    let pregunta4 = new PreguntaModel("¿Conoce el idioma del lugar de destino?", "./assets/imgs/iconos/computador.png");
    pregunta4.crearOpcion("si")
    pregunta4.crearOpcion("no")
    pregunta4.crearOpcion("sininfo")
    preguntas.push(pregunta4);

    let pregunta5 = new PreguntaModel("¿Conoce la dirección del lugar a donde va?", "./assets/imgs/iconos/computador.png");
    pregunta5.crearOpcion("si")
    pregunta5.crearOpcion("no")
    pregunta5.crearOpcion("sininfo")
    preguntas.push(pregunta5);

    let pregunta6 = new PreguntaModel("¿Sabe cuándo va a regresar?", "./assets/imgs/iconos/computador.png");
    pregunta6.crearOpcion("si")
    pregunta6.crearOpcion("no")
    pregunta6.crearOpcion("sininfo")
    preguntas.push(pregunta6);

    let pregunta7 = new PreguntaModel("¿Tiene poco conocimiento del lugar al que se dirige?", "./assets/imgs/iconos/computador.png");
    pregunta7.crearOpcion("si")
    pregunta7.crearOpcion("no")
    pregunta7.crearOpcion("sininfo")
    preguntas.push(pregunta7);

    let pregunta8 = new PreguntaModel("¿Muestra desconfianza hacia las autoridades?", "./assets/imgs/iconos/computador.png");
    pregunta8.crearOpcion("si")
    pregunta8.crearOpcion("no")
    pregunta8.crearOpcion("sininfo")
    preguntas.push(pregunta8);

    let pregunta9 = new PreguntaModel("¿Hizo personalmente los trámites para obtener sus documentos de viaje?", "./assets/imgs/iconos/computador.png");
    pregunta9.crearOpcion("si")
    pregunta9.crearOpcion("no")
    pregunta9.crearOpcion("sininfo")
    preguntas.push(pregunta9);

    let pregunta10 = new PreguntaModel("¿Tiene experiencia en el trabajo que va a realizar en su lugar de destino?", "./assets/imgs/iconos/computador.png");
    pregunta10.crearOpcion("si")
    pregunta10.crearOpcion("no")
    pregunta10.crearOpcion("sininfo")
    preguntas.push(pregunta10);

    let pregunta11 = new PreguntaModel("¿Conoce personalmente a la persona que lo va a recibir?", "./assets/imgs/iconos/computador.png");
    pregunta11.crearOpcion("si")
    pregunta11.crearOpcion("no")
    pregunta11.crearOpcion("sininfo")
    preguntas.push(pregunta11);

    let pregunta12 = new PreguntaModel("¿Quién costeó sus gastos de viaje?", "./assets/imgs/iconos/computador.png");
    pregunta12.crearOpcion("persona")
    pregunta12.crearOpcion("familiar")
    pregunta12.crearOpcion("tercero")
    pregunta12.crearOpcion("sininfo")
    preguntas.push(pregunta12);
    }

    else {

    //PREGUNTAS PARA NIÑOS
    console.log('ninos');

    let pregunta13 = new PreguntaModel("¿Está con adultos que no son sus padres?", "./assets/imgs/iconos/computador.png");
    pregunta13.crearOpcion("si")
    pregunta13.crearOpcion("no")
    pregunta13.crearOpcion("sininfo")
    preguntas.push(pregunta13);

    let pregunta14 = new PreguntaModel("¿Saben sus padres dónde está?", "./assets/imgs/iconos/computador.png");
    pregunta14.crearOpcion("si")
    pregunta14.crearOpcion("no")
    pregunta14.crearOpcion("sininfo")
    preguntas.push(pregunta14);

    let pregunta15 = new PreguntaModel("¿Saben sus padres a dónde se dirige?", "./assets/imgs/iconos/computador.png");
    pregunta15.crearOpcion("si")
    pregunta15.crearOpcion("no")
    pregunta15.crearOpcion("sininfo")
    preguntas.push(pregunta15);

    let pregunta16 = new PreguntaModel("¿El niño sabe el nombre de la ciudad a la que se dirige?", "./assets/imgs/iconos/computador.png");
    pregunta16.crearOpcion("si")
    pregunta16.crearOpcion("no")
    pregunta16.crearOpcion("sininfo")
    preguntas.push(pregunta16);

    let pregunta17 = new PreguntaModel("¿Está contento de estar ahí?", "./assets/imgs/iconos/computador.png");
    pregunta17.crearOpcion("si")
    pregunta17.crearOpcion("no")
    pregunta17.crearOpcion("sininfo")
    preguntas.push(pregunta17);

    let pregunta18 = new PreguntaModel("¿El niño se puede ir si quiere?", "./assets/imgs/iconos/computador.png");
    pregunta18.crearOpcion("si")
    pregunta18.crearOpcion("no")
    pregunta18.crearOpcion("sininfo")
    preguntas.push(pregunta18);
    }

    return preguntas;
  }

  public setTipo(tipo: string) {
    this.tipo = tipo;
    this.preguntas = this.crearPreguntas();

  }

  public darPregunta() {
    if (this.preguntas[this.index]) {
      let respuesta = this.preguntas[this.index];
      this.index = this.index + 1;
      return respuesta;
    }
    else{
    return new PreguntaModel('finPreguntas', '');
    }
  }

  public resetData() {
    this.index = 0;
    this.tipo = '';
  }

  public sendReport (adicional) {

    let rand = Math.floor(Math.random() * 3);
    let riesgo = 'leve';
    if(rand == 2)
    {
      riesgo = 'moderado';
    }
    else if(rand == 1) {
      riesgo = 'bajo';
    }

    let object = {
      ubicacion: 'Bogota D.C',
      riesgo: riesgo,
    }

    if(adicional) {
      object['nombre'] = adicional['nombre'];
      object['identificacion'] = adicional['identificacion'];
      object['ubicacion'] = adicional['ubicacion'];
    }

    console.log('objetoAEnviar', object);

    // const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    this.http.post(this.postURL, object)
    .subscribe(data => {
        console.log(data);
    });
  }
}
