import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MenuPage } from '../menu/menu';
import { DbProvider } from '../../providers/db/db';
import { FormularioReportePage } from '../formulario-reporte/formulario-reporte';

/**
 * Generated class for the EnviarReportePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-enviar-reporte',
  templateUrl: 'enviar-reporte.html',
})
export class EnviarReportePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: DbProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EnviarReportePage');
  }

  infoExtra (moreInfo: boolean) {

    if(moreInfo) {
      this.navCtrl.setRoot(FormularioReportePage);
    }
    else {
      this.sendReport();
      this.navCtrl.setRoot(MenuPage);
    }

  }

  sendReport () {
    this.db.sendReport(false);
  }

}
