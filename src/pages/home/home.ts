import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TestPage } from '../test/test';
import { PreguntaInicialPage } from '../pregunta-inicial/pregunta-inicial';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  goTest() {
    this.navCtrl.push(TestPage);
  }

  irAQuiz() {
    this.navCtrl.push(PreguntaInicialPage);
  }

}
