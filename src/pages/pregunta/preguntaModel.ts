export class PreguntaModel{

    public respondida:boolean;
    public opciones;
    public imagen ="./assets/imgs/computador.png";

	constructor(public pregunta:string, public rutaImagen:string)
	{
        this.respondida = false;
        this.opciones = [];
    }
    
    crearOpcion(textoOpcion:string) {
        this.opciones.push(textoOpcion);
    }
}