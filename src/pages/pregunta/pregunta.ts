import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PreguntaModel } from './preguntaModel';
import { DbProvider } from '../../providers/db/db';
import { EnviarReportePage } from '../enviar-reporte/enviar-reporte';

@Component({
  selector: 'page-pregunta',
  templateUrl: 'pregunta.html',
})
export class PreguntaPage {

  pregunta: PreguntaModel;
  imagen: string;
  imgIndex = 2;
  ultima;
  impar = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public dbProvider: DbProvider) {

    this.pregunta = dbProvider.darPregunta();
    if (this.pregunta.opciones.length % 2 === 1 ) {
      this.impar = true;
      this.ultima = this.pregunta.opciones.pop();
    }
  }

  clickOpcion(param) {
    let vPregunta = this.dbProvider.darPregunta();
    this.setIndex();
    if(vPregunta.pregunta == 'finPreguntas') {
      this.dbProvider.resetData();
      this.navCtrl.setRoot(EnviarReportePage);
    }
    else {
      this.impar = false;
      this.pregunta = vPregunta;
      if (this.pregunta.opciones.length % 2 === 1 ) {
        this.impar = true;
        this.ultima = this.pregunta.opciones.pop();
      }
    }
  }

  setIndex () {
    if(this.imgIndex == 7)
    {
      this.imgIndex = 1;
    }
    else {
      this.imgIndex = this.imgIndex + 1;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreguntaPage');
  }

}
