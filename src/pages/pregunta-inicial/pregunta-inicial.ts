import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { PreguntaPage } from '../pregunta/pregunta';

/**
 * Generated class for the PreguntaInicialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pregunta-inicial',
  templateUrl: 'pregunta-inicial.html',
})
export class PreguntaInicialPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public dbProvider: DbProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreguntaInicialPage');
  }

  preguntasNinos () {
    this.dbProvider.setTipo('ninos');
    this.navCtrl.push(PreguntaPage);

  }

  preguntasAdultos () {
    this.dbProvider.setTipo('adultos');
    this.navCtrl.push(PreguntaPage);
  }

}
