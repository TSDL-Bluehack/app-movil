import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { VerificarOfertaPage } from '../verificar-oferta/verificar-oferta';
import { TestimoniosPage } from '../testimonios/testimonios';
import { FormularioReportePage } from '../formulario-reporte/formulario-reporte';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  goToEsTrata(){
    this.navCtrl.push(HomePage);
  }
  
  goToVerificarOferta() {
    this.navCtrl.push(VerificarOfertaPage);
  }

  goToInformate() {
    this.navCtrl.push(TestimoniosPage);
  }

  goToReportar() {
    this.navCtrl.push(FormularioReportePage);
  }
}
