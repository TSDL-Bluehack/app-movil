import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { MenuPage } from '../menu/menu';

/**
 * Generated class for the FormularioReportePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-formulario-reporte',
  templateUrl: 'formulario-reporte.html',
})
export class FormularioReportePage {

  adicional = {
    nombre: "",
    identificacion: "",
    ubicacion: ""
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: DbProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormularioReportePage');
  }

  onFileChanged(event) {
    const file = event.target.files[0]
  }

  clickEnviar(){
    this.db.sendReport(this.adicional);
    this.navCtrl.setRoot(MenuPage);
  }

}
