import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { MenuPage } from '../menu/menu';
import { OportunidadesPage } from '../oportunidades/oportunidades';

/**
 * Generated class for the VerificarOfertaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-verificar-oferta',
  templateUrl: 'verificar-oferta.html',
})
export class VerificarOfertaPage {

  pais = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerificarOfertaPage');
  }

  validar() {
    var number = Math.random()*10;
    var texto = '';
    if(number < 2) {
      texto = 'Confiable';
    } else if(number < 5){
      texto = 'Leve';
    } else if(number < 7){
      texto = 'Moderado';
    } else {
      texto = 'Alto';
    }
    this.alertCtrl.create({
      title: 'El nivel de Riesgo de la Oferta es: ',
      subTitle: '<br>'+texto,
      buttons: [ {
          text: 'Ver oportunidades validadas',
          handler: () => {
            this.navCtrl.push(OportunidadesPage);
          }
        },
        {
          text: 'Salir',
          handler: () => {
            this.navCtrl.push(MenuPage);
          }
        }
      ]
    }).present();
  }


}
