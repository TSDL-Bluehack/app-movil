import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http'; 
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DbProvider } from '../providers/db/db';
import { TestPage } from '../pages/test/test';
import { PreguntaPage } from '../pages/pregunta/pregunta';
import { PreguntaInicialPage } from '../pages/pregunta-inicial/pregunta-inicial';
import { MenuPage } from '../pages/menu/menu';
import { EnviarReportePage } from '../pages/enviar-reporte/enviar-reporte';
import { FormularioReportePage } from '../pages/formulario-reporte/formulario-reporte';
import { VerificarOfertaPage } from '../pages/verificar-oferta/verificar-oferta';
import { OportunidadesPage } from '../pages/oportunidades/oportunidades';
import { TestimoniosPage } from '../pages/testimonios/testimonios';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TestPage,
    PreguntaPage,
    PreguntaInicialPage,
    MenuPage,
    EnviarReportePage,
    FormularioReportePage,
    VerificarOfertaPage,
    OportunidadesPage,
    TestimoniosPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TestPage,
    PreguntaPage,
    PreguntaInicialPage,
    MenuPage,
    EnviarReportePage,
    FormularioReportePage,
    VerificarOfertaPage,
    OportunidadesPage,
    TestimoniosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DbProvider,
  ]
})
export class AppModule {}
